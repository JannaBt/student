package mk.iwec.rest.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"mk.iwec.rest.controllers", "mk.iwec.rest.service","mk.iwec.rest.dao"})
public class StudentApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentApplication.class, args);
		
		System.out.println("HELLO!");
		
	}
}
