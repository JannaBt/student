package mk.iwec.rest.service;

import java.util.HashMap;

import mk.iwec.rest.model.Student;

public interface StudentService {

	public HashMap<Integer, Student> getAllStudents();
	public Student findbyId(Integer id);
	public void updateStudentbyId(Student student, Integer id);
	public void addStudent(Student student);
	public void removeStudentByIndex(Integer id);

}
