package mk.iwec.rest.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.iwec.rest.dao.StudentRepository;
import mk.iwec.rest.model.Student;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Autowired
	private StudentRepository<Student> students;
	
	@Override
	public void removeStudentByIndex(Integer id) {
		students.removeStudentByIndex(id);
	}
	
	@Override
	public void addStudent(Student student) {
		students.addStudent(student);
	}
	@Override
	public void updateStudentbyId(Student student, Integer id) {
		students.updateStudentbyId(id, student);
	}
	
	@Override
	public HashMap<Integer, Student> getAllStudents() {
		return (HashMap<Integer, Student>) students.getAllStudents();
	}

	@Override
	public Student findbyId(Integer id) {
		return students.findbyId(id);
	}
}
