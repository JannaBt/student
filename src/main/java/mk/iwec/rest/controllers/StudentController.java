package mk.iwec.rest.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import mk.iwec.rest.model.Student;
import mk.iwec.rest.service.StudentService;
import mk.iwec.rest.service.StudentServiceImpl;

@RestController
@RequestMapping("students")
public class StudentController {

	@Autowired
	private StudentService db;

	@GetMapping
	HashMap<Integer,Student> findAll() {
		return db.getAllStudents();
	}
	
	@GetMapping("/{id}")
	Student findById(@PathVariable Integer id) {
		return db.findbyId(id);
	}
	
	@PutMapping("/{id}")
	void updateStudent(@RequestBody Student student, @PathVariable Integer id) {
			db.updateStudentbyId(student, id);
	}
	
	@PostMapping
	void addStudent(@RequestBody Student student) {
		db.addStudent(student);
	}
	
	@DeleteMapping("/{id}")
	void deleteStudent(@PathVariable Integer id) {
		db.removeStudentByIndex(id);
	}
	
}