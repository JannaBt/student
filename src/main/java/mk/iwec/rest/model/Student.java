package mk.iwec.rest.model;

import lombok.AllArgsConstructor;
import lombok.*;

@Data
@AllArgsConstructor
public class Student {
	String firstname;
	String lastname;
	@Override
	public String toString() {
		return firstname + " " + lastname;
	}

}