package mk.iwec.rest.dao;

import mk.iwec.rest.model.Student;
import java.util.HashMap;

import org.springframework.stereotype.Repository;

@Repository
public class StudentRepositoryImpl implements StudentRepository<Student>{
	
	private HashMap<Integer, Student> students = new HashMap<>();
	
	public void removeStudentByIndex(int index) {
		students.remove(index);
	}
	
	public void addStudent(Student student) {
		students.put(students.size(), student);
	}

	public void updateStudentbyId(int index, Student updated) {
		students.replace(index, updated);
	}
	
	public HashMap<Integer,Student> getAllStudents() {
		return students;
	}
	
	public Student findbyId(int id){
		return students.get(id);
	}
}
