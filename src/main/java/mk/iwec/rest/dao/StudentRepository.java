package mk.iwec.rest.dao;

import java.util.Map;
import mk.iwec.rest.model.Student;

public interface StudentRepository<T> {
	public void removeStudentByIndex(int index);
	public void addStudent(T student);
	public void updateStudentbyId(int index, T updated);
	public Map<Integer,Student> getAllStudents();
	public Student findbyId(int id);
}
